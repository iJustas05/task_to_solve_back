<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [\App\Http\Controllers\LoginController::class, 'login']);
Route::post('register', [\App\Http\Controllers\RegisterController::class, 'register']);
Route::post('logout', [\App\Http\Controllers\LoginController::class, 'logout']);
Route::get('accounts', [\App\Http\Controllers\AccountController::class, 'accounts']);
Route::post('transfer', [\App\Http\Controllers\TransferController::class, 'transfer']);
Route::get('incomeHistory', [\App\Http\Controllers\TransferController::class, 'incomeHistory']);
Route::get('outcomeHistory', [\App\Http\Controllers\TransferController::class, 'outcomeHistory']);
