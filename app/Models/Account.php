<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;

    protected $fillable = [
        'owner_id',
        'account_number',
        'amount',
        'currency'
    ];

    public function owner()
    {
        return $this->belongsTo(User::class);
    }
    public function sent()
    {
        return $this->hasMany(Transfer::class, 'sender_account_id');
    }
    public function receive()
    {
        return $this->hasMany(Transfer::class, 'receiver_account_id');
    }
}
