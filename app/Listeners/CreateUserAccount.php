<?php

namespace App\Listeners;

use App\Events\AddStartingMoney;
use App\Events\UserRegistered;
use App\Models\Account;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateUserAccount
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        $account = Account::create([
            'owner_id' => $event->user->id,
            'account_number' => $this->protectedAccountNumber(),
            'amount' => 0,
            'currency' => 'EUR'
        ]);

        AddStartingMoney::dispatch($account);
    }

    private function protectedAccountNumber ()
    {
        return mt_rand(10000000000000, 99999999999999);
    }
}
