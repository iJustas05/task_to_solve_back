<?php

namespace App\Listeners;

use App\Events\AddStartingMoney;
use App\Models\Account;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AddUserMoney
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\AddStartingMoney  $event
     * @return void
     */
    public function handle(AddStartingMoney $event)
    {
        $account = $event->account;
        $amount = $account->amount;
        $amount = $amount + 100000;
        $account->amount = $amount;
        $account->save();
    }
}
