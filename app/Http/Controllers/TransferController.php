<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransferRequest;
use App\Http\Resources\TransferResource;
use App\Models\Account;
use App\Models\Transfer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function Psy\debug;

class TransferController extends Controller
{
    public function transfer(TransferRequest $request)
    {
        $validated = $request->validated();
        $user = Auth::user();
        $sender_account = Account::where('owner_id', '=', $user->id)->firstOrFail();
        $receiver_account = Account::where('account_number', '=', $validated['receiver_account_number'])->firstOrFail();

        if($sender_account->account_number == $validated['receiver_account_number'])
        {
            return response()->json(['message'=>'You cant transfer money to yourself'], 422);
        }

        if($sender_account->amount < $validated['amount'])
        {
            return response()->json(['message'=>'You dont have enough money'], 422);
        } else {
            $sender_account->amount = $sender_account->amount - (int)$validated['amount'];
            $sender_account->save();
            $receiver_account->amount = $receiver_account->amount + (int)$validated['amount'];
            $receiver_account->save();
        }

        $transfer = Transfer::create([
            'sender_account_id' => $sender_account->id,
            'receiver_account_id' => $receiver_account->id,
            'amount' => $validated['amount']
        ]);
        return new TransferResource($transfer);
    }

    public function incomeHistory()
    {
        $user = Auth::user();
        $receiver_account = Account::where('owner_id', '=', $user->id)->firstOrFail();
        $transfersIncome = Transfer::where('receiver_account_id', '=', $receiver_account->id)->get();
        return TransferResource::collection($transfersIncome);
    }

    public function outcomeHistory()
    {
        $user = Auth::user();
        $sender_account = Account::where('owner_id', $user->id)->firstOrFail();
        $transfersOutcome = Transfer::where('sender_account_id', '=', $sender_account->id)->get();
        return TransferResource::collection($transfersOutcome);
    }
}
