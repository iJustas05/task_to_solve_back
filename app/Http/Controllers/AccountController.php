<?php

namespace App\Http\Controllers;

use App\Http\Resources\AccountResource;
use App\Models\Account;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function accounts ()
    {
        $user = Auth::user();
        $accounts = Account::where('owner_id', '=', $user->id)->get();
        return AccountResource::collection($accounts);
    }
}
