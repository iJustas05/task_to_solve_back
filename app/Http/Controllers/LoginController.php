<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => ['required'],
            'password' => ['required']
        ]);

        $attempt = Auth::attempt($request->only('email', 'password'));
        if($attempt) {
            $authenticatable = Auth::user();
            return response()->json($authenticatable, 200);
        }
        return response()->json(['message'=>'Your email or password was incorect'], 422);
    }

    public function logout()
    {
        Auth::logout();
    }
}
