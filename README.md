<h2>Installation</h2>
<hr>

<p>Clone the repository</p>

    git@gitlab.com:iJustas05/task_to_solve_back.git


<p>Switch to the repository folder</p>

    cd task_to_solve_back

<p>Install all the dependencies using composer</p>

    composer install

<p>Copy the example env file and make the required configuration changes in the .env file</p>

    cp .env.example env

<p>Install Laravel Sail</p>

    composer require laravel/sail --dev
    php artisan sail:install

<p>Start Laravel Sail</p>

    ./vendor/bin/sail up

<p>You can access your Laravel server</p>

    http://localhost:8000
